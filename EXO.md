# Springauth - Authentification avec Spring

## Initialisation du projet auth :
Créer un projet springauth avec comme dépendances spring web, spring data jpa, mysql driver, spring devtools, spring actuator, spring validation I/O
1. Créer une entité User qui aura un email en string, un password en string, un id en integer et un rôle en String
2. Créer un repository pour ce user avec JpaRespotiory et dedans rajouter une méthode pour récupérer un seul User par son email
3. Créer un UserController avec une route /api/user en post qui permet de faire persister un user
4. Avant de faire persister le User, faire un find by email pour vérifier si le user existe déjà en base de données, si le optional du find isPresent, alors on renvoie un message d'erreur 400, sinon on fait le persist 

## Le Login :
1. Implémenter l'interface UserDetails sur l'entité User
* implémenter toutes les méthodes de l'interfaces, toutes celles qui commencent par is, on peut leur faire renvoyer true directement
* Le getUsername, on lui fait renvoyer l'email du User
* Le getAuthorities, on va lui faire renvoyer un List.of( new SimpleGrantedAuthority(role))
2. Dans le dossier auth, créer une nouvelle classe AuthService sur laquelle on va mettre une annotation @Service et lui faire implémenter l'interface UserDetailsService
3. Faire un autowired de notre UserRepository et dans la méthode loadUserByUsername, utiliser le findByEmail en lui faisant faire un throw de UsernameNotFoundException si jamais le optional est vide

## Exercice :
1. Rajouter une route en GET qui permet de récupérer tous les utilisateurs sur le projet BACK, sur /api/user/all
2. Dans le projet Angular, ajouter un component AllUsers qui va afficher cette liste dans un tableau HTML classique; dans le tableau, bien utiliser les balises thead, th, tbody, etc

## Suite de l'exercice  :
3. Dans le template Angular du composant AllUsers, afficher le tableau ssi il y a des users dans la liste
4. Dans le projet Java, modifier la class auth.SecurityConfig pour n'autoriser que les utilisateurs ayant le rôle "ROLE_ADMIN" à accéder au endpoint /api/user/all

## Exercice 2 :
1. Dans le projet Back, créer une branche dog, se basculer dessus, et la pusher vers le serveur
2. Dans le projet Front, créer une branche dog, se basculer dessus, et la pusher vers le serveur
3. Dans le projet Back, créer une entité Dog {id (auto-généré), name (string), breed (string) }
4. Modifiez l'entité User pour que chaque User puisse posséder plusieurs chiens (relation OneToMany)
5. Dans le projet Front, créer l'entité Dog
6. Dans le projet Front angular, modifiez le home.component pour afficher la liste des chiens de l'utilisateur connecté, en plus de son email
7. Dans le projet Back, créer le repo et le controller associés à l'entité Dog; le controller doit avoir 1 route : DELETE sur /api/dog/{id} qui supprime un chien
8. Dans le projet Front, rajouter à côté de chaque chien un bouton permettant de le supprimer (il faudra aussi créer le service qui permet de faire la requête vers le back)
9. Dans le projet Back, faites en sorte que l'utilisateur puisse seulement supprimer des chiens qui lui appartiennent 

## Promouvoir un user en admin
1. Créer un contrôleur AdminController dont l'url de base sera /api/admin et dedans, mettre le /user/all que vous aviez mis dans le UserController (et on va juste l'appeler /api/admin/user)
2. Modifier le SecurityConfig pour faire que toutes les routes qui commencent par /api/admin ne soient accessibles qu'aux personnes ROLE_ADMIN
3. Créer une méthode PATCH dans le AdminController, sur la route /api/admin/user/{id}/role/{role}, cette méthode va récupérer le user par son id en utilisant le repository, puis va lui assigner le deuxième argument role en string, comme nouveau role
4. Côté angular, modifier le UserService pour mettre à jour la route pour afficher tous les users
5. Rajouter une méthode changeRole dans ce UserService qui attendra en argument l'id d'un user et le rôle à lui assigner et qui enverra ça sur l'url définie au dessus
6. Dans le composant avec votre list de user, modifier la case rôle pour faire que ça soit un select permettant de choisir entre User (ROLE_USER) et Admin (ROLE_ADMIN)
7. Faire qu'au change sur le select, on déclenche la méthode changeRole
   Bonus : Côté spring, créer un enum UserRoles avec ROLE_USER et ROLE_ADMIN dedans et faire que la méthode pour changer le rôle attende cet enum en argument 

## Changer son mot de passe
1. Dans le UserController, créer une nouvelle route sur /api/user/password en PATCH, avec un RequestBody et avec le user actuellement connecté dans les arguments
2. Dans le dossier auth, créer une nouvelle classe ChangePasswordDto qui aura en propriétés un oldPassword et un newPassword, les deux en string
3. Faire en sorte de typer le RequestBody de la route avec ce Dto, il servira juste à contenir les informations entrantes de notre requête
4. Dans la route changePassword(), utiliser le PasswordEncoder pour faire un matches pour vérifier si le oldPassword correspond au getPassword actuel du user actuellement connecté (récupéré avec un AuthenticationPrincipal). Si c'est pas le cas, on renvoie un bad request
5. Si c'est ok, alors on utilise l'encoder pour hasher le newPassword et l'assigner au user actuellement connecté, qu'on save ensuite
6. Côté front, rajouter dans un des service la méthode changePassword qui va attendre en argument un oldPassword et un newPassword et envoyer ça sous forme d'objet dans le body de la requête patch
7. Créer un nouveau component ChangePassword et le lier à la route /change-password. Dedans, on crée 3 propriétés, oldPassword, newPassword et passwordRepeat qu'on lie aux inputs d'un formulaire dans le template
8. On fait en sorte de disable le button submit et d'afficher un message d'erreur si le newPassword et le passwordRepeat ne correspondent pas
9. Au submit du formulaire, on déclenche la méthode changePassword du service
10. Rediriger vers la page login pour se reconnecter (ou vers la page d'accueil si on veut pas demander une reconnexion) 
