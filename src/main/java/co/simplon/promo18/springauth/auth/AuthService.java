package co.simplon.promo18.springauth.auth;

import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {

  @Autowired UserRepository userRepository;

  /**
   * @param username
   * @return
   * @throws UsernameNotFoundException
   */
  @Override public UserDetails loadUserByUsername(String username)
      throws UsernameNotFoundException {
    Optional<User> maybeUser =  userRepository.findByEmail(username);
    if (maybeUser.isEmpty()) {
      throw new UsernameNotFoundException("User not found");
    }
    return maybeUser.get();
  }
}
