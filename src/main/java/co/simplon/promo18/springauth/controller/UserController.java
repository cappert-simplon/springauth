package co.simplon.promo18.springauth.controller;

import co.simplon.promo18.springauth.auth.ChangePasswordDto;
import co.simplon.promo18.springauth.entity.User;
import co.simplon.promo18.springauth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

  @Autowired private UserRepository repo;

  @Autowired private PasswordEncoder encoder;


  @GetMapping
  public String test(@AuthenticationPrincipal User user) {
    if(user != null) {
      return "Hello "+user.getEmail();
    }
    return "not connected";
  }

  @GetMapping("/account")
  public User getAccount(@AuthenticationPrincipal User user) {
    return user;
  }


  @PostMapping
  public User register(@RequestBody @Valid User user) {
    // check if email already exists in DB
    if (repo.findByEmail(user.getEmail()).isPresent()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists.");
    } else {
      // il ID isn't set as null JPA will do an update()
      user.setId(null);
      // set the role as user, front could send role already
      user.setRole("ROLE_USER");
      String hashed = encoder.encode(user.getPassword());
      user.setPassword(hashed);
      repo.save(user);

      //Optionnel, mais avec ça, on peut connecter le User automatiquement lors de son inscription
      SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));

      return user;
    }

    public List<User> getAll
  }

    @PatchMapping("/password")
    public void changePassword(@RequestBody ChangePasswordDto body, @AuthenticationPrincipal User user) {

      // check request knows old password
      if (!encoder.matches(user.getPassword(), body.getOldPassword())) {
        throw new ResponseStatusException(HttpStatus.FORBIDDEN);
      }
      String hashed = encoder.encode(body.getNewPassword());
      user.setPassword(hashed);
      repo.save(user);
    }
}
