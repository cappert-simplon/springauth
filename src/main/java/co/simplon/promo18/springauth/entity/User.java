package co.simplon.promo18.springauth.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class User implements UserDetails {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @NotBlank
  private String email;

  @NotBlank
  @Size(min=4, max=64)
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) //On ignore le password en Json en lecture pour que le mot de passe ne se balade pas trop ~JD
  private String password;

  private String role;

  @OneToMany(mappedBy = "owner")
  private List<Dog> dogs = new ArrayList<>();

  public User() {
  }

  public User(String email, String password, String role) {
    this.email = email;
    this.password = password;
    this.role = role;
  }

  public User(Integer id, String email, String password, String role) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.role = role;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<Dog> getDogs() {
    return dogs;
  }

  public void setDogs(List<Dog> dogs) {
    this.dogs = dogs;
  }

  /**
   * @return
   */
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(new SimpleGrantedAuthority(role));
  }

  /**
   * @return
   */
  public String getUsername() {
    return email;
  }

  /**
   * @return
   */
  public boolean isAccountNonExpired() {
    return true;
  }

  /**
   * @return
   */
  public boolean isAccountNonLocked() {
    return true;
  }

  /**
   * @return
   */
  public boolean isCredentialsNonExpired() {
    return true;
  }

  /**
   * @return
   */
  public boolean isEnabled() {
    return true;
  }

}
