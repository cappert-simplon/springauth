package co.simplon.promo18.springauth.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Dog {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String name;
  private String breed;

  @ManyToOne
  @JsonIgnore
  private User owner;

  public Dog(Integer id, String name, String breed, User owner) {
    this.id = id;
    this.name = name;
    this.breed = breed;
    this.owner = owner;
  }

  public Dog(String name, String breed, User owner) {
    this.name = name;
    this.breed = breed;
    this.owner = owner;
  }

  public Dog() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBreed() {
    return breed;
  }

  public void setBreed(String breed) {
    this.breed = breed;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }
}
